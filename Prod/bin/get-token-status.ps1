
$access_token = "access_token: (.*?)       token_type:"
 $token_type = "token_type: (.*?) refresh_token:"
 $client_id = "client_id: (.*?) access_token:"
 $file = Get-Item .\context.txt
 $content = Get-Content $file
 
 $token = [PSCustomObject]@{
        Status = 'Good'
        ClientID = [regex]::match($content, $client_id).Groups[1].Value
        Type = [regex]::match($content, $token_type).Groups[1].Value
        Value = [regex]::match($content, $access_token).Groups[1].Value
        URL = $OpsManUrl
    }
    $lastWriteTime = $file.LastWriteTime
    $timespan = New-TimeSpan -Hours 12

    if (((get-date) - $lastwriteTime) -gt $timespan) 
    {
        $token.Status = 'Old'
    }
    else {
        $token.Status = 'Good'
    }

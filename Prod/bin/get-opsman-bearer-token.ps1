
param(
    [Parameter(Mandatory = $true)][string]$OpsManUrl
)

uaac target $OpsManUrl --skip-ssl-validation
uaac token owner get
uaac context | out-file .\context.txt



param(
    [Parameter(Mandatory = $true)][string]$OpsManUrl
)

$OpsManUaa = $OpsManUrl+'/uaa'
. .\get-token-status.ps1

if ($token.Status -eq 'Good')
    {
        Write-Host "This is a good token" -ForegroundColor Cyan
        $bearer_token = '"'+$token.Value+'"'
        curl.exe -k -H "Authorization: bearer $bearer_token" $OpsManUrl/api/v0/certificate_authorities | ConvertFrom-Json | ConvertTo-Json | Out-File '..\manifests\trusted-cert-auth.json'
    }
    else 
    {
        Write-Host "This token is too old..." -ForegroundColor Yellow
        . .\get-opsman-bearer-token.ps1 -OpsManUrl $OpsManUaa
        . .\get-token-status.ps1
        if ($token.Status -eq 'Good')
        {
            Write-Host "This is a good token now..." -ForegroundColor Cyan
            $bearer_token = '"'+$token.Value+'"'
            curl.exe -k -H "Authorization: bearer $bearer_token" $OpsManUrl/api/v0/certificate_authorities | ConvertFrom-Json | ConvertTo-Json | Out-File '..\manifests\trusted-cert-auth.json'
        }
        else 
        {
        Write-Host "Failure has fallen heavily upon you..." -ForegroundColor Yellow
        exit
        }
    }